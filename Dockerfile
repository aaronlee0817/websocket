FROM node:alpine

RUN apk add websocat

EXPOSE 80

ENTRYPOINT websocat -t -E ws-listen:0.0.0.0:80 mirror:
